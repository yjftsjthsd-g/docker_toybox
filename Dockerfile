FROM debian as build
LABEL maintainer "Brian Cole <docker@brianecole.com>"

ARG TOYBOX_VERSION=0.7.8
RUN apt-get update && apt-get install -yq build-essential wget
RUN mkdir -p /opt/toybox/bin
WORKDIR /opt/toybox
RUN wget --no-verbose -O toybox.tar.gz https://www.landley.net/toybox/downloads/toybox-$TOYBOX_VERSION.tar.gz
RUN tar -xvz --strip-components 1 -f toybox.tar.gz
RUN LDFLAGS="--static" make defconfig toybox
RUN PREFIX=/opt/toybox/bin make install_flat

FROM scratch AS final
LABEL maintainer "Brian Cole <docker@brianecole.com>"

COPY --from=build /opt/toybox/bin /bin
WORKDIR /

ENV PATH /bin

ENTRYPOINT ["/bin/toybox"]
