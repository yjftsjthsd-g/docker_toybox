# docker_toybox

In which I create a toybox root image.

Note that at this writing (2020-01-11, toybox version 0.8.2) toybox doesn't seem
to have a working shell, so you can't really use this as a root filesystem and
do anything in it unless you provide your own `/bin/sh` (and probably other
things that I didn't notice).

